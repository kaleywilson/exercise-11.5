import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AlarmClockTester {
	private AlarmClock clock;
	public AlarmClockTester() {
		clock = new AlarmClockImp(11, 32, 6, 15);
	}
	@Test
	public void shouldHandleAll() {
		//show time mode
		assertEquals("11:32", clock.readDisplay());
		// + and - has no effect in show time mode
		clock.increase();
		assertEquals("11:32", clock.readDisplay());
		clock.decrease();
		assertEquals("11:32", clock.readDisplay());
		//switch to set hour mode
		clock.mode();
		assertEquals("06:15", clock.readDisplay() );
		clock.increase();
		clock.increase();
		clock.increase();
		assertEquals("09:15", clock.readDisplay() );
		//switch to set minute mode
		clock.mode();
		clock.decrease();
		clock.decrease();
		clock.decrease();
		clock.decrease();
		clock.decrease();
		assertEquals("09:10", clock.readDisplay());
		
		//go back to show time mode
		clock.mode();
		assertEquals("11:32", clock.readDisplay());
		//remembers the set alarm time
		clock.mode();
		assertEquals("09:10", clock.readDisplay());
	}

}
