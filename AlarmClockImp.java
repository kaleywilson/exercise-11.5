
public class AlarmClockImp implements AlarmClock{
	private Clock realTime;
	private Clock alarmTime;
	private Clock modeClock;
	private int mode;
	
	public AlarmClockImp(int realHr, int realMin, int alarmHr, int alarmMin) {
		realTime = new Clock(realHr, realMin);
		alarmTime = new Clock(alarmHr, alarmMin);
		mode = 1;
		modeClock = realTime;
	}
	
	@Override
	public String readDisplay() {
		return modeClock.getTime();
	}

	@Override
	public void mode() {
		if(mode == 3) {
			mode = 1;
		}else {
			mode++;
		}
		switch(mode) {
		case 1: 
			modeClock = realTime;
			break;
		case 2:
		case 3:
			modeClock = alarmTime;
			break;
		default:
			break;
		}
	}

	@Override
	public void increase() {
		if(modeClock == alarmTime) {
			if(mode == 2) {
				modeClock.increaseHour();
			}else if(mode == 3) {
				modeClock.increaseMinute();
			}
		}
	}

	@Override
	public void decrease() {
		if(modeClock == alarmTime) {
			if(mode == 2) {
				modeClock.decreaseHour();
			}else if(mode == 3) {
				modeClock.decreaseMinute();
			}
		}
	}

}
