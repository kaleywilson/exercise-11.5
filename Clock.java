
public class Clock {
	private int hour;
	private int minute;
	
	public Clock(int hour, int minute) {
		this.hour = hour;
		this.minute = minute;
	}
	
	public String getTime() {
		if(hour < 10 && minute < 10) {
			return "0" + hour + ":0" + minute;
		}else if(hour<10) {
			return "0" + hour + ":" + minute;
		}else if(minute<10) {
			return hour + ":0" + minute;
		}else {
			return hour + ":" + minute;
		}
	}
	
	public void increaseHour() {
		if(hour == 13) {
			hour = 1;
		}else {
			hour++;
		}
	}
	
	public void increaseMinute() {
		if(minute == 60) {
			minute = 0;
		}else {
			minute++;
		}
	}
	
	public void decreaseHour() {
		if(hour == 0) {
			hour = 12;
		}else {
			hour--;
		}
	}
	
	public void decreaseMinute() {
		if(minute == -1) {
			minute = 59;
		}else {
			minute--;
		}
	}
}

